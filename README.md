This project holds a collection of different examples using the CGI REST API

## Configuration
Edit the varaibles in src/Rest.php

## Usage
```
php src/Send/Send.php
```

## Documentation
- [Send REST Api|https://linkmobility.dk/for-udviklere/link-sms-rest-api-send/]
- [Receive REST Api|https://linkmobility.dk/for-udviklere/link-sms-rest-api-recieve/]
- [Gate Api|https://linkmobility.dk/for-udviklere/link-gate-api/]
- [Keyword Api|https://linkmobility.dk/for-udviklere/link-gate-api/]