<?php
namespace App;

// Include the class
require_once __DIR__ . '/../Rest.php';

// Load the class
$rest = new Rest();

// Prepare payload
$payload = [
    'type' => 'PARTNER_GATE',
    'refId' => '',
    'platformId' => $rest->platformId,
    'platformPartnerId' => $rest->platformPartnerId,
    'destinations' => [[
        'contentType' => 'application/json',
        'customParameters' => "",
        'password' => '',
        'url' => $rest->dlrUrl,
        'username' => ''
    ]],
    'gateType' => 'ALL',
    'ttl' => '',
    'acknowledge' => false,
    'throttle' => '',
    'customParameters' => ""
];

// Call the GATE API
$result = $rest->callApi('gate/partnergate', 'POST', $payload, true);

// Handle result
if (isset($result['error'])) {
    $error = json_decode($result['error']);
    echo 'ERROR: got status code: ' . $error->status . ' ' . $error->description . PHP_EOL;
}
preg_match("/Location: .*\/id\/(.*)/", $result['header'], $output);

if (is_array($output)) {
    print_r($output);
} else {
    var_dump($json, $error);
}
