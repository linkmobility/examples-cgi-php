<?php
namespace App;

// Include the class
require_once __DIR__ . '/../Rest.php';

// Load the class
$rest = new Rest();

// Call the Gate API
$result = $rest->callApi("gate/partnergate/platform/{$rest->platformId}/partner/{$rest->platformPartnerId}", 'GET');

// Handle result
if (isset($result['error'])) {
    $error = json_decode($result['error']);
    echo 'ERROR: got status code: ' . $error->status . ' ' . $error->description . PHP_EOL;
}
$json = json_decode($result['body']);
var_dump($json);
