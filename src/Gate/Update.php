<?php
namespace App;

// Include the class
require_once __DIR__ . '/../Rest.php';

// Load the class
$rest = new Rest();

// Prepare payload
$payload = [
    'id' => "[GATEID_FROM_LAST_REQUEST]",
    'type' => 'PARTNER_GATE',
    'platformId' => $rest->platformId,
    'platformPartnerId' => $rest->platformPartnerId,
    'refId' => '',
    'destinations' => [
        'contentType' => 'application/json',
        'customParameters' => [],
        'password' => '',
        'url' => $rest->dlrUrl,
        'username' => ''
    ],
    'gateType' => 'ALL',
    'ttl' => '',
    'acknowledge' => false,
    'throttle' => '',
    'customParameters' => []
];

// Call the GATE API
$result = $rest->callApi('gate/partnergate', 'PUT', $payload);

// Handle result
if (isset($result['error'])) {
    $error = json_decode($result['error']);
    echo 'ERROR: got status code: ' . $error->status . ' ' . $error->description . PHP_EOL;
}
$json = json_decode($result['body']);
var_dump($json);
