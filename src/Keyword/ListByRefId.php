<?php
namespace App;

// Include the class
require_once __DIR__ . '/../Rest.php';

// Load the class
$rest = new Rest();

// Call the GATE API
$result = $rest->callApi("morouter/number/{$rest->number}/keyword/platform/{$rest->platformId}/partner/{$rest->platformPartnerId}/refid/[CREATE_KEYWORD_REFID]", 'GET');

// Handle result
if (isset($result['error'])) {
    $error = json_decode($result['error']);
    echo 'ERROR: got status code: ' . $error->status . ' ' . $error->description . PHP_EOL;
}
$json = json_decode($result['body']);
var_dump($json);
