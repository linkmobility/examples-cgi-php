<?php
namespace App;

// Include the class
require_once __DIR__ . '/../Rest.php';

// Load the class
$rest = new Rest();

// Prepare payload
$payload = [
    'id' => 'RYSIWaIV',
    'type' => 'KEYWORD_ROUTE',
    'refId' => 'RKREF',
    'description' => '',
    'keyword' => 'MYTESTKEYWORD',
    'keywordType' => 'FIRST_WORD',
    'active' => true,
    'number' => $rest->number,
    'start' => gmdate('Y-m-d\TH:i:s\Z'),
    'end' => gmdate('2099-m-d\TH:i:s\Z'),
    'platformId' => $rest->platformId,
    'platformPartnerId' => $rest->platformPartnerId,
    'platformServiceId' => '',
    'platformServiceType' => '',
    'shared' => false,
    'notifyStop' => false,
    'gateIds' => ["$rest->gateId"]
];

// Call the GATE API
$result = $rest->callApi("morouter/number/{$rest->number}/keyword", 'POST', $payload);

// Handle result
if (isset($result['error'])) {
    $error = json_decode($result['error']);
    echo 'ERROR: got status code: ' . $error->status . ' ' . $error->description . PHP_EOL;
}
$json = json_decode($result['body']);
var_dump($json); // NULL means update OK
