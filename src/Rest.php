<?php
namespace App;

class Rest
{
    private $username;
    private $password;
    private $auth;
    public $platformId;
    public $platformPartnerId;

    public function __construct()
    {
        // Basic setup
        date_default_timezone_set('Europe/Copenhagen');
        ini_set("display_errors", 1);
        error_reporting(E_ALL ^ E_NOTICE);

        // Variables to be configured
        $this->username = '';
        $this->password = '';
        $this->destination = '';
        $this->platformId = '';
        $this->platformPartnerId = '';
        $this->number = '';
        $this->apiUrl = 'https://n-eu.linkmobility.io/';
        $this->dlrUrl = '';
        $this->gateId = '';
    }
    
    public function callApi($endpoint, $method, $payload = null, $returnHeaders = false)
    {
        $result = [];
        $this->auth = base64_encode($this->username.':'.$this->password);
        // Headers of the request
        $headers[] = 'Authorization: Basic '.$this->auth;
        $headers[] = 'Accept: application/json';
        if ($payload !== null) {
            $headers[] = 'Content-Type: application/json';
        }
        
        // Set the url to call
        $url = $this->apiUrl . $endpoint;

        // encode the payload to json
        if ($payload != null) {
            $payload_encoded = json_encode($payload);
        } else {
            $payload_encoded = null;
        }
        
        // call api
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_HEADER => true
        ];
        if ($method == 'POST' || $method == 'PUT') {
            $options[CURLOPT_POSTFIELDS] = $payload_encoded;
        }

        $curl = curl_init();
        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);
        
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $result['body'] = substr($response, $header_size);
        if ($returnHeaders !== false) {
            $result['header'] = substr($response, 0, $header_size);
        }
        if (curl_error($curl) != '') {
            $result['error'] = curl_error($curl);
        }
        curl_close($curl);

        return $result;
    }
}
