<?php
namespace App;

// Include the class
require_once __DIR__ . '/../Rest.php';

// Load the class
$rest = new Rest();

// Prepare payload
$payload = [
    'source' => '1272',
    'sourceTON' => 'SHORTNUMBER',
    'destination' => $rest->destination,
    'userData' => 'Hello World!',
    'useDeliveryReport' => true,
    'deliveryReportGates' => [
        $rest->gateId
    ],
    'platformId' => $rest->platformId,
    'platformPartnerId' => $rest->platformPartnerId,
    'customParameters' => [
        'replySmsCount' =>  'true'
    ],
    'tariff' => 100,
    'currency' => 'DKK',
    'productDescription' => 'Informational message from 1272',
    'productCategory' => 15
];

$result = $rest->callApi('sms/send', 'POST', $payload);

// Handle result
if (isset($result['error'])) {
    $error = json_decode($result['error']);
    echo 'ERROR: got status code: ' . $error->status . ' ' . $error->description . PHP_EOL;
}
$json = json_decode($result['body']);
var_dump($json);
