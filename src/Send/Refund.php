<?php
namespace App;

// Include the class
require_once __DIR__ . '/../Rest.php';

// Load the class
$rest = new Rest();

// Prepare payload
$payload = [
    'messageId' => 'nZttGJmGtv5m5roxnG1cR',
    'gateIds' => [
        $rest->gateId
    ]
];

$result = $rest->callApi("sms/refund/platform/{$rest->platformId}/partner/{$rest->platformPartnerId}/msisdn/+4512345678", 'POST', $payload);

// Handle result
if (isset($result['error'])) {
    $error = json_decode($result['error']);
    echo 'ERROR: got status code: ' . $error->status . ' ' . $error->description . PHP_EOL;
}
$json = json_decode($result['body']);
var_dump($json); // NULL means refund operation created OK
