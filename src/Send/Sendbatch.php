<?php
namespace App;

// Include the class
require_once __DIR__ . '/../Rest.php';

// Load the class
$rest = new Rest();

// Prepare payload
$payload = [
    'useDeliveryReport' => false,
    'platformId' => $rest->platformId,
    'platformPartnerId' => $rest->platformPartnerId,
    'sendRequestMessages' => [
    [
        'source' => 'TEST1',
        'destination' => '+4512345678',
        'userData' => 'Hello World1!'
    ],[
        'source' => 'TEST2',
        'destination' => '+4523456789',
        'userData' => 'Hello World2!'
    ]
    ]
];

// Call the CGI REST API
$result = $rest->callApi('sms/sendbatch', 'POST', $payload);

// Handle result
if (isset($result['error'])) {
    $error = json_decode($result['error']);
    echo 'ERROR: got status code: ' . $error->status . ' ' . $error->description . PHP_EOL;
}
$json = json_decode($result['body']);
var_dump($json);
